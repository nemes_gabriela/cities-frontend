import { TestBed } from '@angular/core/testing';
import { HttpClientModule, HttpParams }    from '@angular/common/http';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CityService } from './city.service';
import { MessageService } from './message.service';
import { City } from './city';
import { exec } from 'child_process';


describe('CityService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let cityService: CityService;
  let expectedCities: City[];
  let url: string;

  beforeEach(() => { 
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CityService,
        MessageService
      ]
    });
    
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    cityService = TestBed.get(CityService);

    url = `${cityService.citiesUrl}/cities`;

    expectedCities = [
      { id: 1, name: 'City1', population: 123, votes: 0 },
      { id: 2, name: 'City2', population: 234, votes: 0 },
    ] as City[];
  });

  afterEach( () => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    const service: CityService = TestBed.get(CityService);
    expect(service).toBeTruthy();
  });

  describe('#getCities', () => {
    it('should return expected cities called once', () => {
      cityService.getCities()
        .subscribe( cities => expect(cities).toEqual(expectedCities, 'should return expected cities'), fail);

      const req = httpTestingController.expectOne(`${cityService.citiesUrl}/cities`);
      expect(req.request.method).toEqual('GET');

      req.flush(expectedCities);
    });

    it('should be ok returning no cities', () => {
      cityService.getCities()
        .subscribe( cities => expect(cities.length).toEqual(0, 'should have empty cities array'), fail);

      const req = httpTestingController.expectOne(`${cityService.citiesUrl}/cities`);
      req.flush([]);
    });

    // it('should turn 404 into empty array', () => {
    //   cityService.getCities()
    //     .subscribe( cities => expect(cities.length).toEqual(0,'should return empty heroes array'), fail );

    //   const req = httpTestingController.expectOne(`${cityService.citiesUrl}/cities`);

    //   const msg = 'deliberate 404 error';
    //   req.flush(msg, { status: 404, statusText: 'Not Found'});
    // });

    it('should return getCities called many times', () => {
      cityService.getCities().subscribe();
      cityService.getCities().subscribe();
      cityService.getCities()
        .subscribe( cities => expect(cities).toEqual(expectedCities, 'should return expected cities'), fail );

      const requests = httpTestingController.match(`${cityService.citiesUrl}/cities`);

      expect(requests.length).toEqual(3, 'calls to getCities()');

      requests[0].flush([]);
      requests[1].flush([{id: 1, name: "City1", population: 123}]);
      requests[2].flush(expectedCities);
    });
  });

  describe('#updateCities', () => {
    const makeUrl = (id: number) => `${cityService.citiesUrl}/cities/${id}`;

    it('should update a city and return it', () => {
      const updateCity: City = {id: 1, name: 'UpdatedCity', population: 555, votes: 0};

      cityService.update(updateCity)
        .subscribe( data => expect(data).toEqual(updateCity, 'should return the city'), fail);

      const req = httpTestingController.expectOne(makeUrl(1));
      expect(req.request.method).toEqual('PUT');
      expect(req.request.body).toEqual(updateCity);

      const expectedResponse = new HttpResponse({
        status: 200,
        statusText: '',
        body: updateCity
      })
      req.event(expectedResponse);
    });

    // it('shout turn 404 error into return of the update city', () => {
    //   const updateCity: City = {id: 1, name: 'UpdateCity22', population: 333};

    //   cityService.update(updateCity)
    //     .subscribe( data => expect(data).toEqual(updateCity, 'should return the update hero'), fail);

    //   const req = httpTestingController.expectOne(makeUrl(1));

    //   const msg = 'deliberate error 404';
    //   req.flush(msg, {
    //     status: 404,
    //     statusText: 'Not Found'
    //   });
    // });
  });

  describe('#deleteCity', () => {
    const makeUrl = (id: number) => `${cityService.citiesUrl}/cities/${id}`;

    it('should delete a city and return it', () => {
      const deleteCity: City = {id: 1, name: 'City1', population: 123, votes: 0};

    cityService.delete(deleteCity)
      .subscribe( data => expect(data).toEqual(deleteCity, 'should return the deleted city'), fail);

    const req = httpTestingController.expectOne(makeUrl(1));
    expect(req.request.method).toEqual('DELETE');

    const expectedResponse = new HttpResponse({ 
        status: 200,
        statusText: '',
        body: deleteCity
      });
    req.event(expectedResponse);
    });
  });

  describe('#addCity', () => {
    let newCity: City = {id: 3, name: 'City3', population: 456, votes: 0};

    it('should add a new city and return it', () => {
      cityService.addCity(newCity)
        .subscribe(data => expect(data).toEqual(newCity, 'should return the new city'), fail);

      const req = httpTestingController.expectOne(url);
      expect(req.request.method).toEqual('POST');

      const response = new HttpResponse({
        status: 200,
        statusText: 'OK',
        body: newCity
      });
      req.event(response);
    });
  });

  describe('#searchCity', () => {
    let searchTerm: string;

    it('should found the cities containing City', () => {
      searchTerm = "City";
      cityService.search(searchTerm)
        .subscribe(data => expect(data).toEqual(expectedCities, 'should contain all the cities'), fail);
      
      const req = httpTestingController.expectOne(`${url}/search?name=${searchTerm}`);
      expect(req.request.method).toEqual('GET');

      const response = new HttpResponse({
        status: 200,
        statusText: '',
        body: expectedCities
      });
      req.event(response);
    });
  });

});
