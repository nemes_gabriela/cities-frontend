import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CitiesComponent } from './cities/cities.component';
import { CityDetailComponent } from './city-detail/city-detail.component'
import { CitySearchComponent } from './city-search/city-search.component';
import { CityAddComponent } from './city-add/city-add.component';
import { MessagesComponent } from './messages/messages.component';

const routes: Routes = [
  { path: '', redirectTo:'/cities', pathMatch: 'full' },
  { path: 'cities', component: CitiesComponent },
  { path: 'search', component: CitySearchComponent },
  { path: 'city-detail/:id', component: CityDetailComponent },
  { path: 'add', component: CityAddComponent },
  { path: 'messages', component: MessagesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
