import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { City } from '../city';
import { CityService } from '../city.service';
import { Observable, Subject, of } from 'rxjs';

import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

@Component({
  selector: 'app-city-search',
  templateUrl: './city-search.component.html',
  styleUrls: ['./city-search.component.css']
})
export class CitySearchComponent implements OnInit {

  foundCities$: Observable<City[]>;
  private searchTerms = new Subject<String>();
  
  constructor(private cityService: CityService,
    private location: Location) { }

  ngOnInit() {
    this.foundCities$ = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap( (term: string) => this.cityService.search(term))
    );
  }

  search(term: String){
    this.searchTerms.next(term);
  }

  goBack(): void {
    this.location.back();
  }
}
