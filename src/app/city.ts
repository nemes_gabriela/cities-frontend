export class City {
    id: number;
    name: string;
    population: number;
    votes: number;
}