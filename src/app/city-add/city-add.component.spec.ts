import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule, HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse }    from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { CityAddComponent } from './city-add.component';
import { Data } from '@angular/router';

const testUrl = '/data';

describe('CityAddComponent', () => {
  let component: CityAddComponent;
  let fixture: ComponentFixture<CityAddComponent>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [ CityAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('can test http HttpClient.get', () => {
    const testData: Data = {name: 'Test Data'};

    httpClient.get<Data>(testUrl)
      .subscribe(data => expect(data).toEqual(testData));

    const req = httpTestingController.expectOne('/data');
    expect(req.request.method).toEqual('GET');
    req.flush(testData);
  });

  it('can test HttpClient.get with matching header', () => {
    const testData: Data = {name: 'Test Data'};

    httpClient.get<Data>(testUrl, { headers: new HttpHeaders({'Authorization': 'my-auth-token'})})
      .subscribe(data => expect(data).toEqual(testData));

    const req = httpTestingController.expectOne( req => req.headers.has('Authorization'));
    req.flush(testData);
  });

  it('can test multiple requests', () => {
    const testData: Data[] = [
      { name: 'Test Data1'}, { name: 'Test Data2'},
      { name: 'Test Data3'}, { name: 'Test Data4'}];

    httpClient.get<Data[]>(testUrl)
      .subscribe(d => expect(d.length).toEqual(0, 'should have no data'));

    httpClient.get<Data[]>(testUrl)
      .subscribe(d => expect(d).toEqual([testData[0]], 'should be one element array'));

    httpClient.get<Data[]>(testUrl)
      .subscribe(d => expect(d).toEqual(testData, 'should be expecting data'));

    const requests = httpTestingController.match(testUrl);
    expect(requests.length).toEqual(3);

    requests[0].flush([]);
    requests[1].flush([testData[0]]);
    requests[2].flush(testData);

  });

  it('can test for 404 error', () => {
    const emsg = 'deliberate error 404';

    httpClient.get<Data>(testUrl)
      .subscribe(data => fail('should have failed with the 404 error'),
        (error: HttpErrorResponse) => {
          expect(error.status).toEqual(404, 'status')
          expect(error.error).toEqual(emsg, 'message')
        }
      )

    const req = httpTestingController.expectOne(testUrl);
    req.flush(emsg, { status: 404, statusText: 'Not Found'});
  });

  it('can test for network error', () => {
    const emsg = 'simulated network error';

    httpClient.get<Data>(testUrl)
      .subscribe( data => fail('should have failed with the network error'), 
      (error: HttpErrorResponse) => {
        expect(error.error.message).toEqual(emsg)
      });

    const req = httpTestingController.expectOne(testUrl);

    const mockError = new ErrorEvent('Network error', {
      message: emsg,
      filename: 'city-add.component.ts', // these are not used
      lineno: 42,
      colno: 10
    });

    req.error(mockError);
  });

  it('httpTestingController.verify should fail if not simulated HTTP', () => {
    
    httpClient.get('some/api').subscribe();

    expect(() => httpTestingController.verify()).toThrow();
    const req = httpTestingController.expectOne('some/api');
    req.flush(null);
  });

});
