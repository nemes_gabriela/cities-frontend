import { Component, OnInit } from '@angular/core';
import { CityService } from '../city.service';
import { City } from '../city';
import { Location } from '@angular/common';

@Component({
  selector: 'app-city-add',
  templateUrl: './city-add.component.html',
  styleUrls: ['./city-add.component.css']
})
export class CityAddComponent implements OnInit {

  constructor(private cityService: CityService,
    private location: Location) { }

  ngOnInit() {
  }

  addCity(name: string, population: number): void {
    this.cityService.addCity({name, population} as City)
    .subscribe();
  }

  goBack(): void {
    this.location.back();
  }
}
