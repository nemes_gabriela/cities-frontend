import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request)
        .pipe(catchError( (err: HttpErrorResponse) => {
                if(err.error instanceof Error) {
                    console.error('An error occured (client side): ', err.error.message);
                }
                else {
                    console.log("Backend returned code: ");
                    console.log(err.status);
                    console.log("Body was:");
                    console.log(err.error);
                }
                return EMPTY;
            }));
    }
}
