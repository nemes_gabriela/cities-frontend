import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { City } from './city';
import { MessageService } from './message.service';

import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({'Content-type': 'application/json'})};

@Injectable({
  providedIn: 'root'
})
export class CityService {

  // private heroesUrl = 'api/greetings';
  public citiesUrl = 'http://localhost:8080';


  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  getCities(): Observable<City[]> {
    const url = `${this.citiesUrl}/cities`;
    this.messageService.add('CityService: fetch cities');
    return this.http.get<City[]>(url)
  }

  getCity(id: number): Observable<City> {
    const url = `${this.citiesUrl}/cities/${id}`;
    this.messageService.add('CityService: fetch one city');
    return this.http.get<City>(url)
  }

  update(city: City): Observable<City> {
    const url = `${this.citiesUrl}/cities/${city.id}`;
    this.messageService.add('CityService: update a city');
    return this.http.put<City>(url, city, httpOptions);
  }

  addCity(city: City): Observable<City> {
    const url = `${this.citiesUrl}/cities`;
    this.messageService.add('CityService: add a city');
    return this.http.post<City>(url, city, httpOptions);
  }

  delete(city: City): Observable<City> {
    const url = `${this.citiesUrl}/cities/${city.id}`;
    this.messageService.add('CityService: delete a city');
    return this.http.delete<City>(url, httpOptions);
  }

  search(name: String): Observable<City[]> {
    const url = `${this.citiesUrl}/cities/search?name=${name}`;
    this.messageService.add('CityService: search a city');
    return this.http.get<City[]>(url, httpOptions);
  }
}
