import { Component, OnInit } from '@angular/core';
import { City } from '../city'
// import { HEROES } from '../mock-heroes';
import  { CityService } from '../city.service'

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {

  selectedCity: City;

  cities: City[];
  oneCity: City;

  constructor(private cityService: CityService) { }

  ngOnInit() {
    this.getCities();
  }

  getCities(): void{
    this.cityService.getCities()
    .subscribe(cities => this.cities = cities);
  }

  delete(city: City): void {
    this.cities = this.cities.filter( c => c != city);
    this.cityService.delete(city)
    .subscribe();
  }

  onSelect(city: City): void {
    this.selectedCity = city;
  }

}
