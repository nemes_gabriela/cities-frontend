import { City } from './city';

export const CITIES: City[] = [
    { id: 11, name: 'One', population: 100, votes: 0 },
    { id: 12, name: 'Two', population: 110, votes: 0 },
    { id: 13, name: 'Three', population: 120, votes: 0 },
    { id: 14, name: 'Four' , population: 130, votes: 0},
]